@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('article.index') }}">Articles</a>
        </li>
        <li class="breadcrumb-item active">{{$article->title}}</li>
    </ol>

    <div class="row">
        <div class="card mx-auto col-md-11">
            <div class="card-header">Article preview</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        @if($article->image_path != '' && file_exists($_SERVER['DOCUMENT_ROOT'].$article->image_path))
                            <img src="{{$article->image_path}}"/>
                        @else
                            <img src="/uploads/default.jpg"/>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <p>
                            <b>Title:</b> {{$article->title}}
                        </p>
                        <p>
                            <b>Date:</b> {{ Carbon\Carbon::parse($article->date)->format('d.m.Y') }}
                        </p>
                        <p>
                            <b>Description:</b>
                        </p>
                        {!! $article->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
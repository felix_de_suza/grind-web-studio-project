@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('article.index') }}">Articles</a>
        </li>
        <li class="breadcrumb-item active">Add new article</li>
    </ol>

    <div class="row">
        <div class="card mx-auto col-md-10">
            <div class="card-header">Add new article</div>
            <div class="card-body">
                {!! Form::open(['action' => ['ArticleController@store'], 'enctype' => 'multipart/form-data']) !!}
                <div class="form-group">
                    {!! Form::label('title', 'Enter title'); !!}
                    {!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Enter title', 'required' => true]); !!}
                    @if ($errors->has('text'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('date', 'Enter date'); !!}
                    {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Enter date', 'required' => true]); !!}
                    @if ($errors->has('date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('date') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('image', 'Change image'); !!}
                    {!! Form::file('image', null, ['class' => 'form-control', 'placeholder' => 'Change image']); !!}
                    @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Enter description'); !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']); !!}
                    @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                </div>
                {!! Form::submit('Save', ['class' => 'btn btn-primary btn-block col-md-3']); !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/4.9.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>
@endsection
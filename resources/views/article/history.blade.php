@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('article.index') }}">Articles</a>
        </li>
        <li class="breadcrumb-item active">Article history</li>
    </ol>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Moderator</th>
                <th>Action</th>
                <th>Old data</th>
                <th>New data</th>
                <th>Action date</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($preparedAudits as $audit)
                <tr>
                    <td>{{ $audit->user->name }}</td>
                    <td>{{ $audit->event }}</td>
                    <td>
                        @if(!empty($audit->old_data))
                            @foreach($audit->old_data as $field)
                                <p><b>{{$field['label']}}</b>: {!!  $field['value'] !!}</p>
                            @endforeach
                        @endif
                    </td>
                    <td>
                        @if(!empty($audit->new_data))
                            @foreach($audit->new_data as $field)
                                <p><b>{{$field['label']}}</b>: {!!  $field['value'] !!}</p>
                            @endforeach
                        @endif
                    </td>
                    <td>{{ $audit->date }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $audits->links() }}
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('article.index') }}">Articles</a>
        </li>
        <li class="breadcrumb-item active">List</li>
    </ol>
    <a href="{{ route('article.create') }}" class="btn btn-primary btn-success float-right"><i class="fa fa-plus"></i> Create new article</a>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th style="width: 210px">Image</th>
                <th>Title</th>
                <th>Date</th>
                <th>Author</th>
                <th>Creation date</th>
                <th>Last update</th>
                <th style="width: 150px">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($articles as $article)
                <tr>
                    <td>
                        @if($article->image_path != '' && file_exists($_SERVER['DOCUMENT_ROOT'].$article->image_path))
                            <img src="{{$article->image_path}}" width="200"/>
                        @else
                            <img src="/uploads/default.jpg" width="200"/>
                        @endif
                    </td>
                    <td>{{ $article->title }}</td>
                    <td>{{ Carbon\Carbon::parse($article->date)->format('d.m.Y') }}</td>
                    <td>{{ $article->user->name }}</td>
                    <td>{{ $article->created_at->format('d.m.Y H:i') }}</td>
                    <td>{{ $article->updated_at->format('d.m.Y H:i') }}</td>
                    <td class="operations">

                        <a href="{{route('article.edit', $article->id)}}" class="float-left"><i class="fa fa-edit"></i></a>
                        <a href="{{ route('article.show', $article->id) }}" class="float-left margin-left-20"><i class="fa fa-eye" title="Show"></i></a>
                        <a href="{{route('article.history', $article->id)}}" class="float-left margin-left-20"><i class="fa fa-clock-o"></i></a>
                        {!!Form::model($article, ['route' => ['article.destroy', $article->id], 'method' => 'delete']) !!}
                        <a href="#void" onclick="$(this).parent().submit();" class="float-left margin-left-20"><i class="fa fa-trash"></i></a>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $articles->links() }}
    </div>
@endsection
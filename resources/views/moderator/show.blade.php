@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('moderator.index') }}">Moderators</a>
        </li>
        <li class="breadcrumb-item active">{{$moderator->name}}</li>
    </ol>

    <div class="row">
        <div class="card mx-auto col-md-10">
            <div class="card-header">Moderator details</div>
            <div class="card-body">
                <p>
                    <b>Name:</b> {{$moderator->name}}
                </p>
                <p>
                    <b>Email:</b> {{$moderator->email}}
                </p>
                <p>
                    <b>User role:</b> {{ ($moderator->user_role) ? Config::get('params.userRoles')[$moderator->user_role] : '' }}
                </p>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('moderator.index') }}">Moderators</a>
        </li>
        <li class="breadcrumb-item active">Edit moderator</li>
    </ol>

    <div class="row">
        <div class="card mx-auto col-md-10">
            <div class="card-header">Edit moderator</div>
            <div class="card-body">
                {!!Form::model($moderator, ['route' => ['moderator.update', $moderator->id], 'method' => 'PUT']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Enter Name'); !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name', 'required' => true]); !!}
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Enter Password'); !!}
                    {!! Form::password('password',  ['class' => 'form-control', 'placeholder' => 'Enter Password']); !!}
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Confirm Password'); !!}
                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password']); !!}
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Enter E-Mail Address'); !!}
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter E-Mail Address', 'required' => true]); !!}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('user_role', 'User role '); !!}
                    {!! Form::select('user_role', Config::get('params.userRoles'), null,['class' => 'form-control', 'placeholder' => 'User role', 'required' => true]); !!}
                    @if ($errors->has('user_role'))
                        <span class="help-block">
                            <strong>{{ $errors->first('user_role') }}</strong>
                        </span>
                    @endif
                </div>
                {!! Form::submit('Save', ['class' => 'btn btn-primary btn-block col-md-3']); !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
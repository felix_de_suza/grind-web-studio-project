@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('moderator.index') }}">Moderators</a>
        </li>
        <li class="breadcrumb-item active">List</li>
    </ol>
    <a href="{{ route('moderator.create') }}" class="btn btn-primary btn-success float-right"><i class="fa fa-plus"></i> Create new moderator</a>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>User role</th>
                    <th style="width: 120px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($moderators as $moderator)
                    <tr>
                        <td>{{ $moderator->name }}</td>
                        <td>{{ $moderator->email }}</td>
                        <td>{{ ($moderator->user_role) ? Config::get('params.userRoles')[$moderator->user_role] : '' }}</td>
                        <td class="operations">
                            <a href="{{ route('moderator.edit', $moderator->id) }}" class="float-left"><i class="fa fa-edit" title="Edit"></i></a>
                            <a href="{{ route('moderator.show', $moderator->id) }}" class="float-left margin-left-20"><i class="fa fa-eye" title="Show"></i></a>
                            @if(Auth::user()->id != $moderator->id)
                                {!!Form::model($moderator, ['route' => ['moderator.destroy', $moderator->id], 'method' => 'delete']) !!}
                                <a href="#void" class="float-left margin-left-20 delete"><i class="fa fa-trash" title="Delete"></i></a>
                                {!! Form::close() !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $moderators->links() }}
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $('.delete').click(function () {
            if(confirm("Are you sure you want to delete the moderator? All his articles will be deleted too.")){
                $(this).parent().submit();
            }
        });
    </script>
@endsection
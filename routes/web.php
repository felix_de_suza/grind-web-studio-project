<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/no-access', 'HomeController@noAccess')->name('no-access');
    Route::get('article/{id}/history', 'ArticleController@history')->name('article.history');

    Route::resource('moderator', 'ModeratorController')->middleware('auth.superModerator');
    Route::resource('article', 'ArticleController');
});

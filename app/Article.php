<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Article extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'image_path',
        'date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Array that maps database columns with appropriate labels
     *
     * @var array
     */
    public static $attributeLabels = [
        'title' => 'Title',
        'date' => 'Date',
        'description' => 'Description',
        'image_path' => 'Image',
    ];

    /**
     * Scope a query to only include logged user if he is not super moderator
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param  \App\User $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCurrentUserArticles($query, User $user)
    {
        if(!$user->isSuperModerator()){
            $query->where('user_id', $user->id);
        }

        return $query;
    }

    /**
     * One article has one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * Prepares audit logs for an article to appropriate format
     *
     * @param $audits
     * @return array
     */
    public function prepareAudits($audits){
        $preparedAudits = [];

        if(!$audits->isEmpty()){
            foreach ($audits as $audit){
                $preparedAudits[] = (object)[
                    'user' => $audit->user,
                    'event' => $audit->event,
                    'old_data' => $this->prepareAuditData($audit->old_values),
                    'new_data' => $this->prepareAuditData($audit->new_values),
                    'date' => $audit->created_at->format('d.m.Y H:i'),
                ];
            }

        }

        return $preparedAudits;
    }

    /**
     *  Prepares audit log data field by field
     *
     * @param $data
     * @return array
     */
    public function prepareAuditData($data){
        $preparedData = [];

        if(!empty($data)){
            foreach ($data as $fieldKey => $fieldValue){
                if(!in_array($fieldKey ,array_keys(self::$attributeLabels))){
                    continue;
                }
                $label = (array_key_exists($fieldKey, self::$attributeLabels)) ? self::$attributeLabels[$fieldKey] : $fieldKey;
                $value = '';

                switch ($fieldKey){
                    case 'date':
                        $value = Carbon::parse($fieldValue)->format('Y-m-d');
                        break;
                    case 'image_path':
                        if($fieldValue != '' && file_exists($_SERVER['DOCUMENT_ROOT'].$fieldValue)){
                            $value = '<img src="'.$fieldValue.'" width="200"/>';
                        }
                        break;
                    default:
                        $value = $fieldValue;
                        break;
                }
                $preparedData[] = ['label' => $label, 'value' => $value];
            }
        }

        return $preparedData;
    }
}
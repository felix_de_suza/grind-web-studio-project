<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ModeratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moderators = User::paginate(20);

        return view('moderator.index')->with(array(
            'moderators' => $moderators
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('moderator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email|unique:users',
            'user_role'  => 'required',
            'password'   => 'required|confirmed',

        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('moderator/create')
                ->withErrors($validator)
                ->withInput($request->except(['password']));
        }

        $moderator = new User();
        $moderator->name = $request->get('name');
        $moderator->email = $request->get('email');
        $moderator->password = Hash::make($request->get('password'));
        $moderator->user_role = $request->get('user_role');

        $moderator->save();

        Session::flash('success_message', 'You successfully added moderator');

        return Redirect::to('moderator');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $moderator = User::find($id);

        return view('moderator.show')->with('moderator', $moderator);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $moderator = User::find($id);

        return view('moderator.edit')->with('moderator', $moderator);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email|unique:users,email,'.$id,
            'user_role'  => 'required',
            'password'   => 'confirmed'

        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/moderator/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->except(['password']));
        }

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->user_role = $request->get('user_role');

        if($request->get('password') != ''){
            $user->password = Hash::make($request->get('password'));
        }

        $user->save();

        Session::flash('success_message', 'You successfully edited moderator');

        return Redirect::to('moderator');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        Session::flash('success_message', 'You successfully deleted moderator');

        return Redirect::to('moderator');
    }
}

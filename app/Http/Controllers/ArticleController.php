<?php

namespace App\Http\Controllers;

use App\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Spatie\Glide\GlideImage;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::currentUserArticles(Auth::user())
            ->with('user')
            ->orderBy('id', 'desc')
            ->paginate(20);

        return view('article.index')->with(array(
            'articles' => $articles
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'title'       => 'required',
            'image'       => 'image',
            'date'       => 'required|date',

        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/article/create')
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $article = new Article();
        $article->title = $request->get('title');
        $article->user_id = Auth::user()->id;
        $article->date = Carbon::parse($request->get('date'))->format('Y-m-d');
        $article->description = $request->get('description');

        if($request->file('image')){
            $newImagePath = $_SERVER['DOCUMENT_ROOT'].'/uploads';
            $newImageName = time().'_'.$request->file('image')->getClientOriginalName();
            $request->file('image')->move($newImagePath,$newImageName);
            $article->image_path = '/uploads/'.$newImageName;

            GlideImage::create($newImagePath.'/'.$newImageName)
                ->modify(['w'=> 640])
                ->save($newImagePath.'/'.$newImageName);
        }

        $article->save();

        Session::flash('success_message', 'You successfully added article');

        return Redirect::to('article');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);

        return view('article.show')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        return view('article.edit')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'title'       => 'required',
            'image'       => 'image',
            'date'       => 'required|date',

        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/article/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $article = Article::find($id);
        $article->title = $request->get('title');
        $article->date = Carbon::parse($request->get('date'))->format('Y-m-d');
        $article->description = $request->get('description');

        if($request->file('image')){
            if($article->image_path != '' && file_exists($_SERVER['DOCUMENT_ROOT'].$article->image_path)){
                unlink($_SERVER['DOCUMENT_ROOT'].$article->image_path);
            }
            $newImagePath = $_SERVER['DOCUMENT_ROOT'].'/uploads';
            $newImageName = time().'_'.$request->file('image')->getClientOriginalName();
            $request->file('image')->move($newImagePath,$newImageName);
            $article->image_path = '/uploads/'.$newImageName;

            GlideImage::create($newImagePath.'/'.$newImageName)
                ->modify(['w'=> 640])
                ->save($newImagePath.'/'.$newImageName);
        }

        $article->save();

        Session::flash('success_message', 'You successfully edited article');

        return Redirect::to('article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        Session::flash('success_message', 'You successfully deleted article');

        return Redirect::to('article');
    }

    /**
     *  Show the view for history of an article.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function history($id){
        $article = Article::findOrFail($id);

        $audits = $article->audits()->paginate(20);
        $preparedAudits = $article->prepareAudits($audits);



        return view('article.history')
            ->with('article', $article)
            ->with('audits', $audits)
            ->with('preparedAudits', $preparedAudits)
            ;

    }
}

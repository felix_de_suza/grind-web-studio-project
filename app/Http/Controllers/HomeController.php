<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard with no access message.
     *
     * @return \Illuminate\Http\Response
     */
    public function noAccess()
    {
        Session::flash("error_message", "You don't have access to view this page.");
        return view('home');
    }
}

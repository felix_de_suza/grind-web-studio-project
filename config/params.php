<?php
return [
    'userRoles' => [
        'super_moderator' => 'Super moderator',
        'regular_moderator' => 'Regular moderator'
    ]
];
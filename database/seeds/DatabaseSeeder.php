<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'user_role' => 'super_moderator',
            'password' => \Illuminate\Support\Facades\Hash::make('secret'),
        ]);

        factory(App\User::class, 50)->create()->each(function ($u) {
            $u->articles()->save(factory(App\Article::class)->make());
        });
    }
}
